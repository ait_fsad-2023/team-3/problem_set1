# Require statements can go here if these libraries are used in multiple actions
require 'net/http'
require 'nokogiri'

class MainSiteController < ApplicationController

  # Action to display the main site index
  def index
    readme_path = Rails.root.join('public', 'README.md')  # Construct the full path to README.md in the public directory
    @readme_content = File.read(readme_path)
  end  

  # Action to generate a divide-by-zero exception and log it
  def generate_exception
    begin
      logger.error("About to divide by 0")
      result = 1 / 0 # This will generate a divide-by-zero exception
    rescue ZeroDivisionError => e
      # Log the error
      logger.error("An error occurred: #{e.message}")
      # Render a stack trace to the browser (only for debugging, remove in production)
      render plain: e.backtrace.join("\n")
    end
  end

  # Action to fetch headlines from New York Times and Bangkok Post
  def fetch_headlines
    require 'net/http'
    require 'nokogiri'

    nytimes_url = 'https://www.nytimes.com/section/world'
    bangkok_post_url = 'https://www.bangkokpost.com/world'

    # Fetch and parse New York Times headlines
    nytimes_html = Net::HTTP.get(URI(nytimes_url))
    nytimes_doc = Nokogiri::HTML(nytimes_html)

    # Fetch and parse Bangkok Post headlines
    bangkok_post_html = Net::HTTP.get(URI(bangkok_post_url))
    bangkok_post_doc = Nokogiri::HTML(bangkok_post_html)

    # Hypothetical code to extract headlines using CSS selectors
    # NOTE: Replace 'h2' with the actual CSS selectors for the headlines on each website
    @nytimes_headlines = nytimes_doc.css('h2').map(&:text)
    @bangkok_post_headlines = bangkok_post_doc.css('h2').map(&:text)

    render 'headlines'  # This assumes you have a view file at app/views/main_site/headlines.html.erb
  end

end
