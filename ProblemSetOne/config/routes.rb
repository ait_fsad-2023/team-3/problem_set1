Rails.application.routes.draw do
  get 'main_site/index'
  get 'main_site/generate_exception'
  get 'main_site/fetch_headlines', to: 'main_site#fetch_headlines'
  
  # You can also set 'main_site/index' as your root URL
  root 'main_site#index'
end
